# silkevicious / apkrepo

To add this repo to your F-Droid client, scan the QR code

[![https://codeberg.org/silkevicious/apkrepo/raw/branch/master/fdroid/repo?fingerprint=DFDB0A58E78704CAEB609389B81AB2BE6A090662F860635D760E76ACBC700AF8](fdroid/repo/icons/icon.png)](https://codeberg.org/silkevicious/apkrepo/raw/branch/master/fdroid/repo?fingerprint=DFDB0A58E78704CAEB609389B81AB2BE6A090662F860635D760E76ACBC700AF8)

or copy/paste this URL:

[https://codeberg.org/silkevicious/apkrepo/raw/branch/master/fdroid/repo?fingerprint=DFDB0A58E78704CAEB609389B81AB2BE6A090662F860635D760E76ACBC700AF8](https://codeberg.org/silkevicious/apkrepo/raw/branch/master/fdroid/repo?fingerprint=DFDB0A58E78704CAEB609389B81AB2BE6A090662F860635D760E76ACBC700AF8)

...and add it to F-Droid.

If you would like to manually verify the fingerprint (SHA-256) of the repository signing key, here it is:
```
DF DB 0A 58 E7 87 04 CA EB 60 93 89 B8 1A B2 BE 6A 09 06 62 F8 60 63 5D 76 0E 76 AC BC 70 0A F8
```

**NOTE**: I build the apps directly from their git repos (default branch + latest commit), and eventually the stable release from fdroid. Usually i update it on monday and thursday.

**NOTE 2**: Since i moved to UBPorts i don't need all of these apps. I keep this repo opened mainly to provide a beta testing for [Fediphoto-Lineage](https://codeberg.org/silkevicious/fediphoto-lineage) and to provide [SmartCookieWeb-Preview](https://github.com/CookieJarApps/SmartCookieWeb-preview) to my old Android Phone.

Currently it serves `2` apps:

[com.cookiejarapps.android.smartcookieweb](https://github.com/CookieJarApps/SmartCookieWeb-preview)

[com.fediphoto.lineage](https://f-droid.org/en/packages/com.fediphoto.lineage)