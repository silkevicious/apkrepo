# LICENSES

This repo is licensed under the [![Do What The F*ck You Want To Public License]](https://spdx.org/licenses/WTFPL.html).

This repo is made with [![FDroidServer]](https://gitlab.com/fdroid/fdroidserver) which is licensed with [![GNU Affero General Public License v3.0 or later]](https://spdx.org/licenses/AGPL-3.0-or-later.html)

All the included apps are open source: check their licenses by open their links to F-Droid or their git repositories in the [![README]](https://codeberg.org/silkevicious/apkrepo/src/branch/master/README.md) file.